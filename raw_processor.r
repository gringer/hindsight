#!/usr/bin/env Rscript

sigFileName <- "../pflockingen/signal04.wav";
sigFileName <- "../radio-transmission-recording.wav";

fileLen <- file.size(sigFileName);

data.sig <- readBin(sigFileName, what=integer(), size=2, signed=TRUE,
                    n=fileLen/2);
data.header <- data.sig[1:22];
data.sig <- data.sig[23:length(data.sig)];


showImages <- function(){
    sl=5;
    for(sl in (5:50)*10){
        st <- 1;
        ed <- (st+(sl*sl-1));
        subsig <- data.sig[st:ed];
        image(x=1:sl, y=st + (0:(sl-1))*sl, z=matrix(subsig,sl,sl),
              col=colorRampPalette(c("red","white","blue"))(1000),
              main=sprintf("Samples %d..%s from pfl/04; side length %d",
                           st, ed, sl),
              las=2, xlab="", ylab="",
              useRaster=TRUE);
    }
}

library(gifski);
save_gif(showImages(), gif_file = "../animation.gif", width = 500, height = 500,
         delay = 1, loop = TRUE, progress = TRUE);


data.tern <- ifelse(data.sig < -4000, "A", ifelse(data.sig > 4000, "T", "G"));

cat(">pfl_04\n",
    data.tern, file="../ternary_pflockingen_sig04.fa", sep="", fill=TRUE);

numToDot <- function(val){
    if(val == 0){
        return(matrix(c(0,1,1,0), 2, 2));
    }
    aVal <- abs(val);
    ## work out max number of bits required to represent the number
    bitCount <- max(which(as.integer(intToBits(aVal)) == 1));
    ## work out binary representation
    binRep <- as.integer(intToBits(aVal))[1 : bitCount];
    matWid <- ceiling(sqrt(bitCount));
    ## fill out to square grid size
    newBR <- rep(0, matWid * matWid);
    newBR[1:length(binRep)] <- binRep;
    binRep <- newBR;
    binMat <- t(matrix(binRep, nrow=matWid, ncol=matWid));
    fullMat <- matrix(1, nrow=matWid+1, ncol=matWid+1);
    fullMat[1,1] <- 0;
    fullMat[-1,-1] <- binMat;
    if(val < 0){
        fullMat <- rbind(fullMat, c(1, rep(0, matWid)));
    } else {
        fullMat <- rbind(fullMat, c(0, rep(0, matWid)));
    }
    fullMat <- cbind(fullMat, 0);
    return(fullMat);
}

image(t(rbind(cbind(numToDot(data.sig[ 1]), numToDot(data.sig[ 2]), numToDot(data.sig[ 3]), numToDot(data.sig[ 4])),
              cbind(numToDot(data.sig[ 5]), numToDot(data.sig[ 6]), numToDot(data.sig[ 7]), numToDot(data.sig[ 8])),
              cbind(numToDot(data.sig[ 9]), numToDot(data.sig[10]), numToDot(data.sig[11]), numToDot(data.sig[12])),
              cbind(numToDot(data.sig[13]), numToDot(data.sig[14]), numToDot(data.sig[15]), numToDot(data.sig[16]))
              )),
      ylim=c(1+1/16,0-1/16));

showImages <- function(){
    for(x in 1:64){
        val <- t(numToDot(data.sig[x]));
        image(val[,ncol(val):1], main=sprintf("Signal #1, Sample %d", x),
              xaxt="n", yaxt="n");
    }
}

library(gifski);
save_gif(showImages(), gif_file = "animation.gif", width = 500, height = 500,
         delay = 1, loop = TRUE, progress = TRUE);



