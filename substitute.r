#!/usr/bin/env Rscript

## process 1 - substitute in any elements that only appear once in RHS

data.in <- readLines("galaxy.txt");

sub.appl <- strsplit(data.in, " = ", fixed=TRUE);

RHS <- sapply(sub.appl, function(x){x[2]});
names(RHS) <- sapply(sub.appl, function(x){x[1]});

occurrences <- sapply(names(RHS), function(x){sum(grepl(x, RHS, fixed=TRUE))});

goodSubs <- RHS[names(occurrences[occurrences == 1])];

RHS.mod <- RHS;
for(x in names(goodSubs)){
    RHS.mod <- sub(x, goodSubs[x], RHS.mod, fixed=TRUE);
}

cat(paste(names(RHS.mod), RHS.mod, sep=" = ", collapse="\n"),"\n", sep="",
    file="galaxy_subbed.txt");

## process 2 - substitute in any elements that have no variables on RHS

data.in <- readLines("galaxy.txt");

sub.appl <- strsplit(data.in, " = ", fixed=TRUE);

RHS <- sapply(sub.appl, function(x){x[2]});
names(RHS) <- sapply(sub.appl, function(x){x[1]});

occurrences <- sapply(RHS, function(x){sum(grepl(":", x, fixed=TRUE))});
goodSubs <- RHS[names(occurrences[occurrences == 0])];

RHS.mod <- RHS;
for(x in names(goodSubs)){
    RHS.mod <- gsub(x, goodSubs[x], RHS.mod, fixed=TRUE);
}

cat(paste(names(RHS.mod), RHS.mod, sep=" = ", collapse="\n"),"\n", sep="",
    file="galaxy_subbed.txt");
